import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {
  id = JSON.parse(localStorage.getItem('cities'))[this.route.snapshot.paramMap.get('id')]['id'];
  name;
  country;
  weather;
  map;
  mapbox;
  openstreetmapUrl;
  days;
  selectedDay;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private api: ApiService,
    private sanitizer: DomSanitizer
  ) {}


  ngOnInit() {
    this.getId();
    this.api.forecast(this.id).subscribe(result => {
      this.name = result.city.name;
      this.country = result.city.country;

      this.days = [];

      for (let i = 0; i < 14; i++) {
        let day = new Date((result.list[0].dt + 86400 * i) * 1000);
        this.days.push(day.getFullYear() + '-' + (day.getMonth() + 1) + '-' + day.getDate());
      }

      this.weather = [
        {date: this.days[0], temp: result.list[0].main.temp, wind: result.list[0].wind.speed},
        {date: this.days[1], temp: result.list[8].main.temp, wind: result.list[8].wind.speed},
        {date: this.days[2], temp: result.list[16].main.temp, wind: result.list[16].wind.speed}
      ];

      this.map = {
        lat: result.city.coord.lat,
        lon: result.city.coord.lon
      };

      this.mapbox = {
        x1: (this.map.lon + '').slice(0, 4),
        x2: ((this.map.lon + '').slice(0, 4) * 1) + 1,
        y1: (this.map.lat + '').slice(0, 4),
        y2: ((this.map.lat + '').slice(0, 4) * 1) + 1,
      };

      this.openstreetmapUrl =
        'http://www.openstreetmap.org/export/embed.html?bbox=' +
        this.mapbox.x1 + '%2C' +
        this.mapbox.y1 + '%2C' +
        this.mapbox.x2 + '%2C' +
        this.mapbox.y2 + '%2C' +
        '&layer=mapnik&marker=' +
        this.map.lat + '%2C' +
        this.map.lon;

      this.openstreetmapUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.openstreetmapUrl);
    });
  }

  getId() {
    console.log(typeof this.route.snapshot.paramMap.get('id'));
    if (typeof this.route.snapshot.paramMap.get('id') === 'number') {

    }
    console.log(JSON.parse(localStorage.getItem('cities'))[this.route.snapshot.paramMap.get('id')]['id']);
  }
}
