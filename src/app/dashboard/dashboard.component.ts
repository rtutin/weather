import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  city = '';
  list = [];
  cities = this.getCities();

  constructor(private api: ApiService) {}

  ngOnInit() {
    for (let i in this.cities) {
      let city = this.cities[i];

      this.api.weather(city.id).subscribe(result => {
        this.cities[i] = {
          id: result.id,
          name: result.name,
          country: result.sys.country,
          temp: result.main.temp,
          wind: result.wind.speed
        };

        localStorage.setItem('cities', JSON.stringify(this.cities));
      });
    }
  }

  onKey(event) {
    if (event.target.value.length >= 3) {
      this.api.find(event.target.value).subscribe(result => {
        this.list = result.list;
      });
    }
  }

  onSelect(city) {
    this.list = [];
    this.city = '';
    this.cities.push({
      id: city.id,
      name: city.name,
      country: city.sys.country,
      temp: city.main.temp,
      wind: city.wind.speed
    });

    localStorage.setItem('cities', JSON.stringify(this.cities));
  }

  getCities() {
    let result = JSON.parse(localStorage.getItem('cities'));

    if (result == null) {
      result = [];
    }

    return result;
  }

  remove(index) {
    this.cities.splice(index, 1);
    localStorage.setItem('cities', JSON.stringify(this.cities));
  }
}
