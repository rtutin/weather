import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { City } from './city';

@Injectable()
export class ApiService {
  private apiPath = 'http://api.openweathermap.org/data/2.5/';
  private apiKey = 'a42a11919789986b3b3f587fc9dedeae';
  private results: string[];

  constructor(private http: HttpClient) { }

  find(q): Observable<any> {
    return this.http.get(this.apiPath + 'find', {
      params: {
        q: q,
        type: 'like',
        appid: this.apiKey,
        units: 'metric'
      }
    }).pipe(
      tap(data => {
        console.log(`fetched cities`);
      }),
      catchError(this.handleError<any>('search', []))
    );
  }

  weather(id): Observable<any> {
    return this.http.get(this.apiPath + 'weather', {
      params: {
        id: id,
        appid: this.apiKey,
        units: 'metric'
      }
    }).pipe(
      tap(data => {
        console.log(`fetched city ` + data['name']);
      }),
      catchError(this.handleError<any>('search', []))
    );
  }

  forecast(id): Observable<any> {
    return this.http.get(this.apiPath + 'forecast', {
      params: {
        id: id,
        appid: this.apiKey,
        units: 'metric'
      }
    }).pipe(
      tap(data => {
        console.log(`forecast by ` + data['name']);
      }),
      catchError(this.handleError<any>('search', []))
    );
  }

  forecastDaily(id): Observable<any> {
    return this.http.get(this.apiPath + 'forecast/daily', {
      params: {
        id: id,
        units: 'metric',
        appid: this.apiKey
      }
    }).pipe(
      tap(data => {
        console.log(`forecast by ` + data['name']);
      }),
      catchError(this.handleError<any>('search', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
